import os
from sqlalchemy.orm import (
    sessionmaker, scoped_session
)
from sqlalchemy import (
    create_engine
)
import uuid
import binascii
from dotenv import load_dotenv
from pathlib import Path


# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Reading .env file
ENV_PATH = os.path.join(BASE_DIR, 'env')
env_path = os.path.join(Path(ENV_PATH), '.env')
load_dotenv(dotenv_path=env_path)

# Statement for enabling the development environment
DEBUG = os.getenv("DEV_DEBUG")


# Define the database - we are working with
# SQLite for this example


class Config(object):
    DB_CONFIG = "mysql://{0}:{1}@{2}:{3}/{4}".format("root", "nuage@123", "192.168.1.203", 3306, "dbrentospace")


dbEngine = create_engine(Config.DB_CONFIG, pool_size=20, isolation_level="READ COMMITTED")
dbEngine.echo = False
dbEngine.pool_pre_ping = True
session_factory = sessionmaker(bind=dbEngine, autoflush=True)
dbSession = scoped_session(session_factory)

# Connect to database session
DB = dbSession()


# Uid from new table data
def UUID():
    return binascii.hexlify(uuid.uuid4().bytes).decode('ascii')


# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"

SMTP_MAIL_CONFIG = {
    'EMAIL_HOST_USER': "",
    'EMAIL_HOST_PASSWORD': "",
    'EMAIL_HOST': "",
    'EMAIL_PORT': 465,
    'EMAIL_SETTINGS': "ssl"
}
