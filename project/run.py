# Run a test server.
from flask_cors import CORS
from pathlib import Path
import sys
from app import app

# Resolved related path issue
file = Path(__file__).resolve()
parent, top = file.parent, file.parents[3]

sys.path.append(str(top))


if __name__ == '__main__':
    CORS(app)
    app.run(host='0.0.0.0', port=8080, debug=True, threaded=True)
