# Import flask and template operators
from flask import Flask, render_template

# Import a module / component using its blueprint handler variable (mod_auth)
from .module_one.roughts import mod_auth as auth_module

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template(''), 404


# Register blueprint(s)
app.register_blueprint(auth_module)
# app.register_blueprint(xyz_module)
# ..

# Build the database:
# This will create the database file using SQLAlchemy
# db.create_all()
