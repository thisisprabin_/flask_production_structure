from flask import (
    Blueprint,
)
from .controllers import (
    sign_in_controller,
)

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint('auth', __name__, url_prefix='/auth')


# Set the route and accepted methods
@mod_auth.route('/sign_in/', methods=['POST'])
def sign_in():
    return sign_in_controller()
