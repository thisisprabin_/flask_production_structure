# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from sqlalchemy import (
    BINARY, Column, DECIMAL, ForeignKey, INTEGER, SMALLINT, String, DateTime, Text, Time, text, DateTime
)
from MySQLdb.compat import unicode
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import VARCHAR
from sqlalchemy import func

# Define a base model for other database tables to inherit


class Base(object):
    @property
    def to_dict(self):
        """
        Jsonify the sql alchemy query result. Skips attr starting with "_"
        """
        cls = self.__class__
        convert = {"DATETIME": datetime.datetime.timestamp}
        d = dict()
        for c in cls.__table__.columns:
            if c.name.startswith("_"):
                continue
            v = getattr(self, c.name)
            current_type = str(c.type)
            if current_type in convert.keys() and v is not None:
                try:
                    d[c.name] = convert[current_type](v)
                except Exception as e:
                    d[c.name] = "Error:  Failed to covert using ", unicode(convert[c.type])
            elif v is None:
                d[c.name] = unicode()
            else:
                d[c.name] = v
        return d


Base = declarative_base(cls=Base)
metadata = Base.metadata


class IDQUID(VARCHAR):

    def bind_expression(self, bindvalue):
        # convert the bind's type from HEX to BINARY encoded
        return func.UNHEX(bindvalue)

    def column_expression(self, col):
        # convert select value from BINARY To HEX
        return func.HEX(col)


# Define a User model
class User(Base):

    __tablename__ = 'auth_user'

    id = Column(IDQUID(32), primary_key=True)

    # User Name
    name = Column(String(128), nullable=False)

    # Identification Data: email & password
    email = Column(String(128), nullable=False, unique=True)
    password = Column(String(192), nullable=False)

    # Authorisation Data: role & status
    role = Column(SMALLINT, nullable=False)
    status = Column(SMALLINT, nullable=False)
    date_created = Column(DateTime, default=func.current_timestamp())
    date_modified = Column(DateTime, default=func.current_timestamp(), onupdate=func.current_timestamp())

    # New instance instantiation procedure
    def __init__(self, name, email, password):

        self.name = name
        self.email = email
        self.password = password

    def __str__(self):
        return "{} - {}".format(self.id, self.email)
